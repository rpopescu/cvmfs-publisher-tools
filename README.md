# CernVM-FS publisher tools

## Overview

Additional tools and services for publishing to CernVM-FS repositories.

## License and copyright

See LICENSE in the project root.